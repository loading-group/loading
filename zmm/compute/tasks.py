from compute.planner import Planner
from celery import shared_task
from .app import app
from .planner import Planner
import copy

def fake_compute(dataset):
    import copy
    import time
    time.sleep(30)
    return copy.deepcopy(dataset)

def real_compute(payload):
    data=copy.deepcopy(payload)
    #payload -> algo_type,num_beams,selected_container,input_dataset
    planner=Planner(algo_type=data['config']['algo_type'],
        num_beams=data['config']['num_beams'],
        selected_container=data['config']['container'])
    return planner.compute(data['input_dataset'])



@shared_task(name='zmm.do_compute')
def on_task_compute(payload):
    try:
        #通知计算任务已经开始
        app.send_task('zmm.do_start',[payload])

        #执行计算
        #TODO：实现真正的计算逻辑，现在搞个假的
        #result=fake_compute(payload['input_dataset'])
        result=real_compute(payload)

        #通知计算任务已经结束
        payload['result_dataset']=result
        app.send_task('zmm.do_finish',[payload])
    except Exception as e:
        #通知计算任务已经出错
        app.send_task('zmm.do_fail',[payload])
        raise e