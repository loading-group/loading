import os
from celery import Celery
from kombu import Queue


app = Celery('compute')


# 只启动一个worker进程（没有并行）
app.conf.work_concurrency = 1


# 从环境变量中读取Redis相关配置
_password = os.environ['REDIS_PASSWORD']
_broker_addr = os.environ['REDIS_HOST']
_port = os.environ['REDIS_PORT']


# 设置broker地址
app.conf.broker_url = f'redis://:{_password}@{_broker_addr}:{_port}/0'

# 设置该Celery Worker 应该监听的队列
app.conf.task_queues = [
    Queue('zmm.request.queue'),
]

# 设置任务发送的目标队列
app.conf.task_routes = {
   # 假设发送的任务消息名都以”zmm.“开头
    'zmm.*': {
        # 发送请求队列
        'queue': 'zmm.results.queue'
    },
}