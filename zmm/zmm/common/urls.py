from django.urls import path
from . import views


urlpatterns = [
    path('api/register/', views.RegisterView.as_view()),
    path('api/login', views.LoginView.as_view()),
    #---
    path('api/users/',views.UserViewSet.as_view({
        'get':'list',
        'patch':'partial_update',
    })),
    #--------
    path('api/users/<str:pk>/',views.UserViewSet.as_view({
        'delete':'destroy',
    })),
]
