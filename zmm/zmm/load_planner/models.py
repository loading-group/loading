from re import T
from django.db import models
from django.db.models.fields import TextField


class ShippingContainer(models.Model):
    name=models.TextField(help_text='运柜的名字，比如20GP')

    length=models.FloatField(help_text='运柜的长度')
    width=models.FloatField(help_text='运柜的宽度')
    height=models.FloatField(help_text='运柜的高度')

    created=models.DateTimeField(auto_now_add=True,help_text='数据创建时间戳')


from enum import Enum
class TaskStatus(Enum):
    IDLE='idle'#闲置状态
    SUBMITTED='submitted'#已提交，但还未开始计算
    RUNNING='running'#正在计算
    FINISHED='finished'#未知原因导致计算失败
    ERROR='error'

    @classmethod
    def choices(cls):
        return tuple(
            (i.value,i.name)
            for i in cls
        )
    
class Task(models.Model):

    creator=models.ForeignKey(
        #User,#会造成循环，以及去掉顶部的from commmon，models import
        'common.User',
        related_name='tasks',
        on_delete=models.CASCADE,
        help_text='该任务的创建者',
        null=True,blank=True
    )

    #可更改属性1
    name=models.TextField(help_text='任务名称')

    status=models.TextField(
        choices=TaskStatus.choices(),
        default=TaskStatus.IDLE.value,
        help_text='任务状态'
    )
    
    #可更改属性2
    config=models.JSONField(
        help_text='算法参数',
        null=True,
        blank=True
    )

    
    
    #可更改属性3
    input_dataset=models.JSONField(
        help_text='算法参数',
        null=True,
        blank=True
    )
    result_dataset=models.JSONField(
        help_text='算法结果数据',
        null=True,
        blank=True
    )



    created_at=models.DateTimeField(auto_now_add=True,help_text='任务的创建时间')
    #---------------
    submitted_at=models.DateTimeField(help_text='任务计算的提交时间',null=True,blank=True)
    finished_at=models.DateTimeField(help_text='任务计算的结束时间',null=True,blank=True)
    #-------------------