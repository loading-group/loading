from django.urls import path
from . import views

urlpatterns={
    path('api/tasks/',views.TaskViewSet.as_view({
            'get': 'list',
            'post':'create',
        })),
    path('api/tasks/<str:pk>/',views.TaskViewSet.as_view({
            'get': 'retrieve',
            'patch':'partial_update',
            'delete':'destroy',
        })),

    path('api/shipping-containers/',
        views.ShippingContainerViewSet.as_view({
            'get': 'list',
            'post':'create',

        }),name='containers')
}
