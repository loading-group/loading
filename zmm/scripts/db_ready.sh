#!/bin/bash

if [ -z "$POSTGRES_USER" ]; then
    export POSTGRES_USER=postgres
fi

# If not DB is set, then use USER by default
if [ -z "$POSTGRES_DB" ]; then
    export POSTGRES_DB=$POSTGRES_USER
fi

# Need to update the DATABASE_URL if using DOCKER
export DATABASE_URL=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_DB:$POSTGRES_PORT/$POSTGRES_DB

# Wait for postgres server to be available
function postgres_ready(){
python << END
import sys
import psycopg2
try:
    conn = psycopg2.connect(dbname="$POSTGRES_DB", user="$POSTGRES_USER", password="$POSTGRES_PASSWORD", host="$POSTGRES_HOST")
    # cur = conn.cursor()
    # cur.execute("GRANT USAGE ON SCHEMA public TO root;")
    # cur.execute("GRANT CREATE ON SCHEMA public TO root;")

except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 5
done
