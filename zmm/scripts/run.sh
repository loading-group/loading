#等待postgresql数据库
./scripts/db_ready.sh

#启动UI
python -m ui.index &


#启动计算Celery
celery -A compute worker -l INFO &

cd zmm

if [ "$DJANGO_RESET_DB" = true ]; then
    echo 'Completely resetting database... all previous data will be dropped!'
    python manage.py reset_db
fi


# Migrate ZMM apps

#迁移Django自己的数据模型
#为我们自己的Django app迁移模型
python manage.py makemigrations common
python manage.py tenant_migrate --public yes

python manage.py makemigrations load_planner
python manage.py tenant_migrate

#启动API端的Celery worker
celery -A celery_client worker -l INFO &

# Start ZMM apps
python manage.py runserver 0.0.0.0:8080