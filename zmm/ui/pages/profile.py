import dash
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
from dash.dependencies import Input, Output, State

from .. import api_client
from ..app import app


card_info = dbc.Card(dbc.CardBody(
    dbc.Container([
        # 邮箱
        dbc.Row([
            dbc.Col([
                html.B('邮箱:', style={'font-size': '20px'})
            ], width=3),
            dbc.Col([
                html.Span('', style={'vertical-align': 'middle'}, id='email')
            ], width=6)
        ], style={'width': '350px'}),
        # 姓名
        dbc.Row([
            dbc.Col([
                html.B('姓名:', style={'font-size': '20px'})
            ], width=3),
            dbc.Col([
                html.Span('', style={'vertical-align': 'middle'}, id='full-name')
            ], width=6)
        ], style={'width': '350px', 'marginTop': 5}),
        # 角色
        dbc.Row([
            dbc.Col([
                html.B('角色:', style={'font-size': '20px'})
            ], width=3),
            dbc.Col([
                html.Span('', style={'vertical-align': 'middle'}, id='role')
            ], width=6)
        ], style={'width': '350px', 'marginTop': 5}),
        # 注册时间
        dbc.Row([
            dbc.Col([
                html.B('注册时间:', style={'font-size': '20px'})
            ], width=3),
            dbc.Col([
                html.Span('', style={'vertical-align': 'middle'}, id='register-time')
            ], width=8)
        ], style={'width': '500px', 'marginTop': 5}),
    ])
), style={'width': '600px', 'margin': '60px 70px'})

card_buttons = dbc.Card(dbc.CardBody(
    dbc.Container([
        html.Button(
            children='登出',
            id='logout-btn',
            className='btn btn-primary btn-lg',
            style={'marginRight': 40}
        ),
        html.Button(
            children='一键改名',
            type='submit',
            id='change-name-btn',
            className='btn btn-primary btn-lg'
        ),
    ])
), style={'width': '600px', 'margin': '-40px 70px'})

content = dbc.Container([
    dcc.Location(id='profile-location', refresh=True),
    dbc.Row(card_info),
    dbc.Row(card_buttons)
], id='profile-page')


def make_random_name():
    import random
    first_names = ["秀","娟","英","华","慧","巧","美","娜","静","淑","惠","珠","翠","雅","芝","玉","萍","红","娥","玲","伟","刚","勇","毅","俊","峰","强","军","平","保","东","文","辉","力","明","永","健","世","广","志","义","兴","良"]
    last_names = ["赵", "钱", "孙", "李", "周", "吴", "郑", "王", "冯", "陈", "褚", "卫", "蒋", "沈", "韩", "杨"]
    return random.choice(last_names), random.choice(first_names) + random.choice(first_names)


# Callbacks
# ---------
@app.callback(
    Output('email', 'children'),
    Output('full-name', 'children'),
    Output('role', 'children'),
    Output('register-time', 'children'),
    Input('profile-page', 'children'),
    Input('change-name-btn', 'n_clicks'),
)
def on_landed(_, __):
    ctx = dash.callback_context
    prop_id = ctx.triggered[0]['prop_id'].split('.')[0]
    user_data = api_client.get_user()

    if prop_id == 'change-name-btn':
        rand_last_name, rand_first_name = make_random_name()

        user_data = api_client.update_user(
            id=user_data['id'],
            payload={
                'first_name': rand_first_name,
                'last_name': rand_last_name,
            }
        )

    return (
        user_data['email'],
        ' '.join([user_data['last_name'], user_data['first_name']]),
        ','.join(user_data['roles']),
        user_data['created'],
    )


@app.callback(
    Output('profile-location', 'pathname'),
    Input('logout-btn', 'n_clicks'),
    State('profile-location', 'pathname')
)
def on_logout(n_clicks, curr_path):
    if not n_clicks:
        raise dash.exceptions.PreventUpdate('stop')
    
    dash.callback_context.response.delete_cookie('access_token')
    return '/login'
