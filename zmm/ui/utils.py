import json
from typing import Any, List


def force_list(obj: Any) -> List[Any]:
    if not isinstance(obj, list):
        return [obj]
    return obj


def ensure_json(obj: Any) -> str:
    if isinstance(obj, dict):
        return json.dumps(obj)
    return obj
